# Angular


## Component



## Data Binding
Quiźa el concepto más importante de Angular. 
  ### Interpolación y property Data Binding
  Nuestra aplicación será una composición de componentes. Uno principal uno que filtra y otro que agrupa las charlas etc. 
  La interpolación *dinámica* se usa con el double moustache ({{}}) y tiene dos sintaxis **interpolación** y property **data binding**: 
  
   Queremos usar la interpolación dinámica (recordar que la de backtics + ${}) es estática. 
    Al igual que en vue, cuando el valor cambie en el componente, se reflejará en el template. 

    ```

    @Component({
      template : `
      {{title}}
      `,
    })
    export class TalksComponent implements OnInit {
      title: string

      constructor() { 
        this.title = 'All about Angular'
      }

      
    }

    ```

    Recordar que se han quitado partes del componente para subir sólo los cambios. 
    Hay que tener también en cuenta que, de momento, no es un buen ejemplo de interpolación, ya que está reflejando un valor de manera estática (coge el valor del constructor y lo pinta)

    El modo más fácil es usando un timeout, le decimos que en 3 segundos cambie title:

    ```
    constructor() { 
        this.title = 'All about Angular'

        setTimeout(() =>
          this.title = 'All about Angular has ben cancelled',
          3000
        )
      }

    ```

    Como hemos indicado, el cambio viene de un setTimeout, pero podría venir de un evento en el DOM, de una petición de base de datos etc. 

    Existe una sintaxis equivalente : 

    ```
    <p textContent ="{{title}}"></p>

    ```

    Tendría el mismo resultado. Se llama interpolación. 


    Esta última a su vez sería equivalente a hacerlo así : 


    ```
    <p [textContent]= "title"></p>

    ```
    Este modo se llama data binding


    Hacerlo al modo de vue no funciona: 

    ```
    <p>{{title}</p>

    ```

    Esto renderizaría {{title}} tal cual en la web. 

    De momento sólo hemos cambiado textContent, pero se puede cambiar cualquier valor de html. 

  - disabled ={{expression}} o su equivalente [disabled]="expression" ==> En este primer ejemplo, se deshabilitará el contenido de una etiqueta si se cumple una expresión. 
  - <button [style.color] = "isSpecial ? 'red' : 'green'">

   Los atributos que no tienen su equivalente en propiedades DOM no pueden ser cambiados dinámicamente (aria,svg, table span... )

   Class y style tienen ligeras diferencias. 






### HTML atributos o propiedades DOM
Viene del Data Binding : Qué está haciendo Angular por detrás? 
Se están cambiando propiedades del árbol DOM. Es igual que cambiar con getElementById. 

Qué diferencia hay entre etiqueta html y elemento DOM? 
Las etiquetas html sólo inicializan propiedades. Si se cambia la propiedad del DOM sí cambia automaticamente. 




### Metadata
Dentro de @component.
- TemplateUrl enlaza el componente a un archivo html. 
- Template permite crear directamente el html. 

Por lo tanto 

```templateUrl: './app.component.html'```

y

```
Template: `
  <h1>
  {{title}} 
  </h1> `
```  
Serían lo mismo si el contenido de app.component.html es el h1 con title. 


## Module
Los módulos son puntos de entrada a la app. Formas de trabajar con la app. Conceptos de negocio.


### Template expression
Podríamos definirlo como la manera de comunicarse o el pequeño lenguaje que usamos con los double moustache. 
Es similar a Javascript y no únicamente se puede llamar a variables, también a métodos. Como en vue las expresiones son evaluadas y se convierten en cadena. 
NO se permiten efectos secundarios (new(), ++ ) ni namespace global (Math., window...) 
Puesto que las expresiones van a ser evaluadas, las expresiones deberían ser rápidas (Angular no garantiza que se evaluen más de una vez), simples (no deberían involucrar muchos objetos) e idempotentes (siempre debería devolver lo mismo si no se ha tocado el valor subyacente). 

EN nuestro ejemplo cambiamos el color de un talk: 

Dentro de template : 

```
<p [textContent]= "title3" [style.color]="getColor()"></p>

```

Dentro de la clase (fuera del constructor):

```
 getColor(){
    return 'blue'
  }
  
```