# Anotaciones sobre TypeScript

El el lenguaje recomendado para trabajar con Angular por varios motivos: 
- Soporta ES6
- Encaja con la filosofía de Angular (los tipos ayudan  a saber si el desarrollador se está confundiendo)
- Permite usar decoradores (entre otras cosas relacionan componentes con templates)


## Características de Ecmascript 6 que Typescript soporta nativamente
Veamos un ejemplo de template ya conocido
```
 template: `
  <h1>
  {{title}} 
  Created by ${author}
  
  </h1>
  `
```
Title está dentro de la clase y author está definida como una variable fuera. Por este motivo title es dinámico y author no. Si en el constructor de clase
definiésemos un setInterval para que cambiase el valor de title y author pasado unos segundos, sólo title cambiaría. 


**Rest y Spread**: 
```
let array = [2,3,5]

console.log(Math.max(... array)) 
```

Sin los ... nos devolvería error porque Math.max espera valores. 



```
let array = [2,3,5]
let secondArray = [6, 7, 9]

console.log([...array, ...secondArray]) 
```
El console log daría [2,3,5,6,7,9]



```
constructor(){
    this.print(1,2,3)
}
print(... args){
    console.log(args)
}
```
El console log daría [1, 2, 3]
Los tres puntos dicen que se capturen todos los argumentos que se le pasen y cree una array en base a ellos. 


**Destructuración**:

```
let array = [0,1,2]
let [a,b] = array

console.log('variable a: ' + a), `variable b: ${b}`

```
El console log sería 'variable a: 0 variable b: 1'
El argumento 2 sería ignorado 


```
let array = [0,1,2]
let [a, ...b] = array

console.log('variable a: ' + a), `variable b: ${b}`

```
La variable a sería 0 y la variable b un array con 1 y 2. 



```
let array = {a:0, b:1}
let {a, ...b} = array

console.log('variable a: ' + a), `variable b: ${b}`

```
variable a: 0 variable b: [object Object] (como tiene el spread, es el resto de variables que tenga el objeto)


Además de descomponer arrays y objetos, con el destructuring se pueden *invertir valores*. 

```
let a = 0
let b = 1

[a,b] = [b, a]

console.log('variable a: ' + a), `variable b: ${b}`

```
Variable a = 1, variable b = 0



También podemos *transformar argumentos de entrada en una función*

```
constructor (){
    this.print([0,1])
}


print(a,b){
console.log('variable a: ' + a), `variable b: ${b}`
}

```
Typescript se quejaría porque print espera dos valores y le llega una array. 


```
constructor (){
    this.print([0,1])
}


print([a,b]){
console.log('variable a: ' + a), `variable b: ${b}`
}

```

Así sí funcionaría.




