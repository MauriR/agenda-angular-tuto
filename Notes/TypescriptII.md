# Características *Exclusivas* de Typescript

Lo que vimos en Typescript.md, también era aplicable a Javascript. 

## Tipos en Typescript.

Tenemos los siguientes: boolean | number | string | array | enum | any | void

Los no tan conocidos:

- **enum** es una enumeración
- **any** puede ser cualquier tipo
- **void** es la ausencia de tipo. 

El compilador siempre va a intentar adivinar el tipo de la variable. Es un soporte gradual. 

```
export class AppComponent {
    title: any

    constructor(){
        this.title = 2
        this.title = "2"
    }

}

```

Le estamos diciendo que la variable va a ser de cualquier tipo, que no haga ningún trabajo. 

Si le quitásemos el tipo any (quedaría sólo la palabra title), se daría cuenta por sí mismo que se le está asignando tipos distintos, por lo tanto él sólo declararía el any. 


Sin embargo si se le asigna el valor a title: 

```
export class AppComponent {
    title = '2'

    constructor(){
        this.title = 2
        this.title = "2"
    }

}

```

this.title = 2 daría fallo porque no es una cadena y se ha interpretado que el tipo va a ser string. TS va a inferir que se ha creado title para trabajar con strings. 

Si queremos un valor pero no trabajar únicamente con strings, podríamos declarar title así:

```
title : any = '2'
```

## Arrays

```
export class AppComponent {
    title : Array<any>

    constructor(){
        this.title = 2
    }

}

```

Este caso daría error porque no es un Array. 


```
export class AppComponent {
    title : Array<any>

    constructor(){
        this.title = [2, '2']
    }
}

```

Este funcionaría. 



```
export class AppComponent {
    title : Array<number> // o su equivalente  number[]

    constructor(){
        this.title = [2, '2']
    }
}

```

Este fallaría porque typescript se daría cuenta de que le estamos pasando una cadena ('2'). 


## Enumeraciones 

```
enum TaklCategory {Workshop, talk}

```

Son 'pseudoconstantes' dentro de las cuales, nuestras variables sólo pueden aceptar valores dentro de esta numeración. 


## Interfaces 
**!!!** *intentar entender mejor*

Un interfaz es una lista de acciones que puede llevar a cabo un determinado objeto.
¿Diferencia con  los métodos que se definen en una clase?  En una clase además de aparecer los métodos aparecía el código para dichos métodos, en cambio en un interfaz sólo existe el prototipo de una función, no su código.
Ejemplo: Pensemos en un interfaz que en su lista de métodos aparezcan los métodos despegar, aterrizar, servirComida y volar. Todos pensamos en un avión, ¿verdad? El motivo es sencillamente que avión es el concepto que engloba las acciones que hemos detallado antes, a pesar que existan muchos objetos avión diferentes entre sí, por ejemplo Boeing 747, Boeing 737, MacDonell-Douglas.

Lo realmente interesante es que todos ellos, a pesar de pertenecer a clases distintas, poseen el interfaz avión, es decir poseen los métodos detallados en la lista del interfaz avión.

Esto significa también que a cualquier avión le podemos pedir que vuele, sin importarnos a que clase real pertenezca el avión, evidentemente cada clase especificará como volará el avión (porque proporciona el código de la función volar).

```
interface Talk {
    title: string
}
```

Las interfaces se pueden implementar. 
Si tomamos la clase de los ejemplos anteriores, vamos a implementar la interfaz Talk : 

```
export class AppComponent implements Talk {
    title 

    constructor(){
        this.title = [2, '2']
    }
}

```

Otro modo de implementación sería el siguiente: 

```
export class AppComponent {
    title : Talk

    constructor(){
        this.title = {title: 'Android'}
    }
}

```
Si el valor de this.title era [2,'2'] se quejaba. 

Si se crease otro tipo en nuestra interfaz: 

```
interface Talk {
    title: string
    description : string
}
```
La clase AppComponent daría fallo porque le faltaría un campo. 


## Visibilidad 

Si no se indica nada, la visibilidad es pública. Sin embargo se puede declarar como privada: 

```
export class AppComponent {
    private title : Talk

    constructor(){
        this.title = {title: 'Android'}
    }
}

```

Le estamos diciendo que la visibilidad de la clase es privada. De este modo otro objeto que usase AppComponent no pudiese hacer un AppComponent instancia .title


## Constructores Breves

```
export class AppComponent {

    constructor(public name = 'Rx'){
        
    }
}

```
Equivale a definir un atributo y fijar su valor. 
Es equivalente a esto : 

```
export class AppComponent {
    name

    constructor(name = 'Rx'){
        this.name= name
    }
}

```

## Operator Elvis

También se le llama null safe operator. 

Recuperemos un chunk de hace un momento: 

```
template:
    <h1>
      {{talk.title}}
    <h1>

 export class AppComponent {
    talk: Talk

    constructor(){
        this.talk= {title:'Rx'}
    }
}   

```

Esto pintaría 'Rx' en nuestra pantalla. 

Sin embargo, si nuestra charla no tuviese nada.

```
template:
    <h1>
      {{talk.title}}
    <h1>

 export class AppComponent {
    talk: Talk

    constructor(){
        
    }
}   

```

Al no existir title, la aplicación petaría. 


```
template:
    <h1>
      {{talk?.title}}
    <h1>

 export class AppComponent {
    talk: Talk

    constructor(){
        
    }
}   

```

Con el interrogante, operator elvis, le decimos que si está vacío, lo muestre vacío, que no intente mostrar el valor. Es una especie de if(value) value; else ''

Se pueden concatenar distintos niveles con el elvis operator: objeto?.propiedad?.valor



