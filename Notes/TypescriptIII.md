# Más Diferencias con ES6

Typescript evoluciona más rápido que ES6. 

**Tipos Unión**

```

export class AppComponent {
    talk: 'North' | 'East' 

    constructor(){
        this.talk = "Rx"
    }
}
```
Fallaría porque si bien talk es una cadena, no es ni 'North 'ni 'East' que son los dós únicos valores que permitimos. 


**Mixins**

Soportados en typescript. Sirven para reutilizar código sin recurrir a la herencia (¿algo del cuadrado?). Decora objetos con comportamientos concretos extraídos de interfaces. 

