import { Component } from '@angular/core';
const author = 'Mauri'

@Component({//metadata
  selector: 'app-root',
  template: `
  <h1>
  {{title}} 
  Created by ${author}
  
  </h1>
  <app-talks> </app-talks>
  `,
  // templateUrl: './app.component.html',      //app.component es el componente inicial y renderiza app.component.html si se cambia title, dentro de la clase, cambia en el html
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Agenda';
}
