import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-talks',
  template : `
  {{title}}
  <p textContent ="{{title2}}" ></p>
  <p [textContent]= "title3" [style.color]="getColor()"></p>
  `,
  styleUrls: ['./talks.component.css']
})
export class TalksComponent implements OnInit {
  title: string
  title2: string
  title3: string
  getColor(){
    return 'blue'
  }

  constructor() { 
    this.title = 'All about Angular'
    this.title2 = 'Angular vs. Vue'
    this.title3 = 'Angular Rocks!'

    setTimeout(() =>
      this.title = 'All about Angular has ben cancelled',
      3000
    )

    setTimeout(() =>
    this.title2 = 'Angular vs. Vue has changed hours: mon. 13th 18:45',
    3000
  )

  setTimeout(() =>
  this.title3 = 'Assistants to Angular Rocks! talk, do not forget to bring pen and paper',
  3000
)

  }
  

  ngOnInit(): void {
  }

}
