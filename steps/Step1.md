# Creando nuestro primer coponente

Vamos a crear el componente (el que nos crea por defecto no lo queremos). Para ello usaremos el cliente de Angular. 
Al clienta se accede con el siguiente comando en consola:


```
ng generate component Talks

```

o su forma abreviada: 

```
ng g c Talks

```


Recordar que *Talks* es el nombre de nuestro componente. 
Si vamos al árbol, veremos que se ha creado el nuevo componente, se llama TalksComponent y tiene un asrie de métodos por defecto : 


```

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-talks',
  templateUrl: './talks.component.html',
  styleUrls: ['./talks.component.css']
})
export class TalksComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

```

El template  (talks.component.html) es muy sencillo (un <p> con talks works dentro)

Sin embargo lo que vamos a hacer es un template inline. 

En nuestro componente vamos a sustituír templateUrl por template. Y el valor de template va a ser `Talks funciona!!`

Si vamos ahora a ver nuestra página, veremos que no se está mostrando. Sigue estando la información antigua. 
Lo que tenemos que hacer es ir a nuestro componente raíz (app) y vemos que en template estamos renderizando un template. 
Lo que necesitamos es usar nuestro selector (ver línea 28 de este archivo). Este selector es la manera de llamar a nuestro componente desde otros archivos. 
Dentro del template de appComponent se puede hacer referencia al selector abriendo y cerrando la etiqueta: 

```
import { Component } from '@angular/core';
const author = 'Mauri'

@Component({//metadata
  selector: 'app-root',
  template: `
  <h1>
  {{title}} 
  Created by ${author}
  
  </h1>
  <app-talks> </app-talks>
  `,
  // templateUrl: './app.component.html',      //app.component es el componente inicial y renderiza app.component.html si se cambia title, dentro de la clase, cambia en el html
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Agenda';
}

```

A partir de este punto se intentará poner sólo  el código de los cambios, no todo el archivo. 

*Apunte, a mí me funciona, pero el ID del autor del curso marca el componente y el mío no. Igual que no me cambia de color las cosas de dentro del template.*

Si se tienen dudas sobre interpolación y data binding, hay una entrada en Diccionario.md con el nombre **Data Binding**

Al final del vídeo, el archivo queda así: 

```
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-talks',
  template : `
  {{title}}
  <p textContent ="{{title2}}" ></p>
  <p [textContent]= "title3" [style.color]="getColor()"></p>
  `,
  styleUrls: ['./talks.component.css']
})
export class TalksComponent implements OnInit {
  title: string
  title2: string
  title3: string
  getColor(){
    return 'blue'
  }

  constructor() { 
    this.title = 'All about Angular'
    this.title2 = 'Angular vs. Vue'
    this.title3 = 'Angular Rocks!'

    setTimeout(() =>
      this.title = 'All about Angular has ben cancelled',
      3000
    )

    setTimeout(() =>
    this.title2 = 'Angular vs. Vue has changed hours: mon. 13th 18:45',
    3000
  )

  setTimeout(() =>
  this.title3 = 'Assistants to Angular Rocks! talk, do not forget to bring pen and paper',
  3000
)

  }
  

  ngOnInit(): void {
  }

}

```






